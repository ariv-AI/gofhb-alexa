import os
import boto3
import cryptography
HEALTHBOOK_URL = os.environ["HEALTHBOOK_URL"]
DUCKLING_URL= os.environ["DUCKLING_URL"]
USE_DEFAULT_IDS = os.environ["USE_DEFAULT_IDS"]
#import subprocess
from base64 import b64decode
from cryptography.fernet import Fernet
AUTH_KEY_ENCRYPTED = os.environ["AUTH_KEY"]
f = Fernet(AUTH_KEY_ENCRYPTED)
# Read auth from s3 bucket
s3 = boto3.client("s3")
bucket = "ai-gofhb-auth"
key_user = "user_auth.txt"
user_auth_en = s3.get_object(Bucket=bucket, Key=key_user)
user_auth_en = user_auth_en["Body"].read()
user_auth = f.decrypt(user_auth_en).decode("utf-8")
