import os
from config import sheela_config
import boto3
import cryptography
#import subprocess
from base64 import b64decode
import jwt
import requests
import json
from cryptography.fernet import Fernet
BASE_URL=sheela_config.HEALTHBOOK_URL
AUTH_KEY_ENCRYPTED = os.environ["AUTH_KEY"]
def get_fhb_info(handler_input):
    if sheela_config.USE_DEFAULT_IDS == "YES":
        doctor_id = os.environ["DOCTOR_ID"]
        statuscode = 200
        user_auth = sheela_config.user_auth
        print("doc,user",doctor_id,user_auth)
        return(statuscode,str(doctor_id), str(user_auth))
    else:
        url = BASE_URL + "auth/get-token"
        accesstoken=handler_input.request_envelope.context.system.user.access_token
        statuscode = 200
        if accesstoken is None:
            statuscode = 201
            doctor_id = None
            user_auth = None
            return(statuscode,doctor_id,user_auth)

        #print("accesstoken",accesstoken)
        header = {"Content-Type": "application/json"}
        body = {"source":"goFHBAlexa","token":accesstoken}
        get_fhbid=requests.post(url,headers=header,data=json.dumps(body))
        user_auth=get_fhbid.json()
        if user_auth['result'] == "":
            statuscode = 202
            doctor_id = None
            user_auth = None
            return(statuscode,doctor_id,user_auth)
        decode_auth=jwt.decode(user_auth['result'],algorithms=['RS256'],verify=False)
        try:
            doctor_id = decode_auth['token']['contextId']
        except:
            statuscode = 202
            doctor_id = None
            user_auth = None
            return(statuscode,doctor_id,user_auth)
        return(statuscode,doctor_id,user_auth['result'])



#Function to convert AM and PM to string format 
def convert24(str1): 
    str1 = str1.replace(" ", "")
    if str1[-2:] == "am" and str1[:2] == "12": 
        return "00" + str1[2:-2] + ":00"  
    elif str1[-2:] == "am": 
        return str1[:-2] + ":00"   
    elif str1[-2:] == "pm" and str1[:2] == "12": 
        return str1[:-2]     
    else: 
        return str(int(str1[:2]) + 12) + str1[2:5] +':00'