from ask_sdk_core.api_client import DefaultApiClient
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
import ask_sdk_core.utils as ask_utils
from ask_sdk_core.skill_builder import CustomSkillBuilder
from ask_sdk_model.dialog import ElicitSlotDirective
from ask_sdk_model import (Intent , IntentConfirmationStatus, Slot, SlotConfirmationStatus)
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model.services import ServiceException
from ask_sdk_core.utils import is_intent_name, get_dialog_state, get_slot_value
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model import Response, DialogState
from ask_sdk_model.services.reminder_management import (Trigger, TriggerType,
                                                        AlertInfo, SpokenInfo,
                                                        SpokenText,
                                                        PushNotification,
                                                        PushNotificationStatus,
                                                        ReminderRequest)
from ask_sdk_model.ui import AskForPermissionsConsentCard,Card
from ask_sdk_model.interfaces.connections.v1.start_connection_directive import StartConnectionDirective
from ask_sdk_model import Response
import logging
import pytz
import datetime
from datetime import timedelta
import requests
import json
import jwt
import pandas as pd
import os
from config import sheela_config
from utils import sheela_utils
from config import static_responses
intent1=None
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
BASE_URL=sheela_config.HEALTHBOOK_URL
DUCKLING_URL=sheela_config.DUCKLING_URL



class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        #url="https://<your-user-pool-domain>/oauth2/userInfo"
        accesstoken=handler_input.request_envelope.context.system.user.access_token
        USE_DEFAULT_IDS = os.environ["USE_DEFAULT_IDS"]
        if USE_DEFAULT_IDS == 'NO':
            if accesstoken is None:
                speak_output = static_responses.ACCOUNT_LINK
                return (handler_input.response_builder.speak(speak_output).set_card(Card('LinkAccount')).set_should_end_session(True).response)
            url = BASE_URL+ "auth/get-token"
            accesstoken=handler_input.request_envelope.context.system.user.access_token
            #print("accesstoken",accesstoken)
            header = {"Content-Type": "application/json"}
            body = {"source":"goFHBAlexa","token":accesstoken}
            get_fhbid=requests.post(url,headers=header,data=json.dumps(body))
            user_auth=get_fhbid.json()
            if user_auth['result'] == "":
                speak_output = static_responses.PROFILE_SETUP
                return (handler_input.response_builder.speak(speak_output).set_should_end_session(True).response)
            decode_auth=jwt.decode(user_auth['result'],algorithms=['RS256'],verify=False)
            try:
                doctor_id = decode_auth['token']['contextId']
            except:
                speak_output = static_responses.PROFILE_SETUP
                return (handler_input.response_builder.speak(speak_output).set_should_end_session(True).response)
        else:
            # print("get_fhbid",get_fhbid1, get_fhbid.status_code)
            speak_output = static_responses.LAUNCH_REQUEST

            return (handler_input.response_builder.speak(speak_output).set_should_end_session(False).response)
class Byeintent(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_intent_name("byeintent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = static_responses.BYE_INTENT

        return (handler_input.response_builder.speak(speak_output).set_should_end_session(True).response)


class Rescheduleintent(AbstractRequestHandler):
    """Handler for Reschedule Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_intent_name("rescheduleintent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        multiple_date = None
        time_zone="Asia/Kolkata"
        slots = handler_input.request_envelope.request.intent.slots
        #intent1=handler_input.request_envelope.request.intent.name
        session_attr = handler_input.attributes_manager.session_attributes
        statuscode,doctor_id,user_auth=sheela_utils.get_fhb_info(handler_input)
        if statuscode == 201:
            output = static_responses.ACCOUNT_LINK
            return (handler_input.response_builder.speak(output).set_card(Card('LinkAccount')).set_should_end_session(True).response)
        elif statuscode == 202:
            output = static_responses.PROFILE_SETUP
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)


        # To handle query redirect for reschedule
        try:
            source=session_attr.get("source")
            booking_id=session_attr.get("booking_id")
            #reschedule_date = session_attr.get("reschedule_date")
            secondtext_value=slots['secondtext']
            secondtext1=secondtext_value.value
            if source == 'querying':
                # Checking whether it has date to reschedule --  otherswise using elicit slot to send and receive the date from user 
                if secondtext1 is None:
                    output='please tell me when should i reschedule'
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).add_directive(
                        directive=ElicitSlotDirective( 
                            slot_to_elicit="secondtext")).response)
                # Using the text and and getting time from duckling
                if secondtext1 is not None:
                    url=DUCKLING_URL
                    payload = {"tz":time_zone,"text":secondtext1}
                    output = requests.post(url, data=payload)
                    duckling=output.json()
                    reschedule_timestamp=duckling[0]['value']['values'][0]['value']
                    reschedule_date = str(datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                    session_attr["source"]='reschedule'
                    session_attr["reschedule_date"]=reschedule_date
                    session_attr["booking_id"]=booking_id
                    # This return is for verifyig the voice password by alexa
                    return handler_input.response_builder.add_directive(StartConnectionDirective(uri='connection://AMAZON.VerifyPerson/1',input={})).response
            else:
                pass
        except:
            pass       
        
        
        time_text_value=slots['text']
        secondtext_value=slots['secondtext']
        time_text=time_text_value.value
        secondtext=secondtext_value.value
        timezone = pytz.timezone('Asia/Kolkata')
        #slots =[]
        list_of_text=[]
        # Checking whether it has date when the doctor is looking for -- Otherwise asking the date using elicit slot
        if time_text is None:
            output='please tell me when is your appointment to reschedule'
            return (handler_input.response_builder.speak(output).set_should_end_session(False).add_directive(
                directive=ElicitSlotDirective( 
                    slot_to_elicit="text")).response)
        # Asking the date to reschedule using elicit slot
        if secondtext is None:
            output='please tell me when should i reschedule'
            return (handler_input.response_builder.speak(output).set_should_end_session(False).add_directive(
                directive=ElicitSlotDirective( 
                    slot_to_elicit="secondtext")).response)
        session_attr["source"]='reschedule'
        url=DUCKLING_URL
        payload1 = {"tz":time_zone,"text":secondtext}   ## Using duckling to get date values
        resp1 = requests.post(url, data=payload1)
        duckling1=resp1.json()
        reschedule_date1=duckling1[0]['value']['values'][0]['value']
        reschedule_date=str(datetime.datetime.strptime(reschedule_date1,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
        present_date=str(datetime.datetime.now(timezone).date())
        if reschedule_date < present_date:
            output=static_responses.PAST_DATE_RESCHEDULE
            return (handler_input.response_builder.speak(output).set_should_end_session(False).add_directive(
                directive=ElicitSlotDirective( 
                    slot_to_elicit="secondtext")).response)

        session_attr["reschedule_date"]=reschedule_date
        payload = {"tz":time_zone,"text":time_text}
        output = requests.post(url, data=payload)
        duckling=output.json()
        reschedule_timestamp = None
        between_till_flag = None
        try:
            reschedule_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':duckling[0]['value']['values'][0]['from']['value']}
        except:
            pass
        if reschedule_timestamp is None:
            try:
                reschedule_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':None}
            except:
                try:
                    reschedule_timestamp={'to':None,'from':duckling[0]['value']['values'][0]['from']['value']}
                except:
                    pass
        if reschedule_timestamp is None:
            try:
                reschedule_timestamp=duckling[0]['value']['values'][0]['value']
            except:
                pass
        # Checking the type of timestamp
        if isinstance(reschedule_timestamp, str):
            date = str(datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
            only_time = str(datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            headers = {'authorization': user_auth ,'Content-Type': 'application/json'}
            params={'calendarType':'daily','doctorId':doctor_id,'isCount':'false'}
            url=BASE_URL+'appointment/filter'
            body={
            "dates": [
                date
            ]
            }
            request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
            print("status code of fileter_api_str in reschedule",request.status_code)
            if request.status_code == 401:
                output = static_responses.UNAUTHORISED_USER
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
            get_booking_id = request.json()
            # Appending all the booking id to a list
            booking_id=[]
            booking_id=[]
            # Checking whether the uer has given time
            if only_time == '00:00:00':
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            # Checking whether user have asked from one particular time 
            elif 'from' in time_text.split():
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if only_time < get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            # If user has given time
            else:
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if only_time == get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            # If doctor has no appointment
            if len(booking_id) == 0:
                output = "Doc, there are no appointments on the mentioned date"
                # With no time
                if reschedule_timestamp[11:19] == '00:00:00':
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta (days=1)
                    if reschedule_timestamp[:10] == str(current):
                        output = static_responses.NO_APPOINTMENTS_TODAY
                    elif reschedule_timestamp[:10] == str(tomorrow):
                        output = static_responses.NO_APPOINTMENTS_TOMORROW
                    elif reschedule_timestamp[:10] == str(day_after_tomorrow):
                        output = static_responses.NO_APPOINTMENTS_DAY_AFTER_TOMORROW
                    else:
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            output = "Doc, there are no appointments {}.".format(time_text)
                        else:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are no appointments on {}.".format(date_time)
                # With time from user
                else:
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta(days=1)
                    date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                    if reschedule_timestamp[:10] == str(current):
                        output = "Doc, there are no appointments today at {}".format(date_time)
                    elif reschedule_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are no appointments tomorrow at {}.".format(date_time)
                    elif reschedule_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are no appointments day after tomorrow at {}.".format(date_time)
                    else:
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                            output = "Doc, there are no appointments {} at {}.".format(time_text,date_time)
                        else:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are no appointments on {}. ".format(date_time)
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

            # If doctor has appointment
            else:
                print('booking id',booking_id)
                # Sending booking id's to session attributes
                session_attr["source"]='reschedule'
                session_attr["booking_id"]=booking_id
                # Response with out time
                if reschedule_timestamp[11:19] == '00:00:00':
                    if len(booking_id) == 1:
                        len_book = 'appointment'
                    else: 
                        len_book = 'appointments'
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta (days=1)
                    # Responses for today,tomorrow and day after tomorrow
                    if reschedule_timestamp[:10] == str(current):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"today. Should I proceed to reschedule the appointments?"
                    elif reschedule_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book +" "+"tomorrow. Should I proceed to reschedule the appointments?"
                    elif reschedule_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"day after tomorrow. Should I proceed to reschedule the appointments?"
                    else:
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            output = "Doc, there are {}".format(len(booking_id))+" "+" "+len_book+" "+"{}. Should I proceed to reschedule the appointments?".format(time_text)
                        else:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"on {}. Should I proceed to reschedule the appointments?".format(date_time)
                    print(output)
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
                else:
                    if len(booking_id) == 1:
                        len_book = 'appointment'
                    else: 
                        len_book = 'appointments'
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta(days=1)
                    date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                    if reschedule_timestamp[:10] == str(current):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"today at {}.".format(date_time)+"Should I proceed to reschedule the appointments?"
                    elif reschedule_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"tomorrow at {}.".format(date_time)+"Should I proceed to reschedule the appointments?"
                    elif reschedule_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"day after tomorrow at {}.".format(date_time)+"Should I proceed to reschedule the appointments?"
                    else:
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                            output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"{} at {}. Should I proceed to reschedule the appointments?".format(len(booking_id),time_text,date_time)
                        else:
                            date_time = datetime.datetime.strptime(reschedule_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are {}".format(len(booking_id)) + " "+len_book +" "+" on {}.".format(date_time) + "Should I proceed to reschedule the appointments?"
                    print(output)
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).response)

        elif isinstance (reschedule_timestamp, dict): 
            # Checking the type of timestamp
            slots=[] 
            # Fetching sessions with Doctor id
            date_list=[]
            startdate = reschedule_timestamp['from']
            starttime = reschedule_timestamp['from']
            enddate = reschedule_timestamp['to']
            endtime = reschedule_timestamp['to']
            # Checking condition with the timestamp from slots and assigning a variable for future usage
            # Handling between
            if startdate is not None and enddate is not None:
                startdate = str(datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                starttime = str(datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                # If user says till of future date , duckling gives output has from and to ..so handling till in between with this flag 
                if starttime == "00:00:00":
                    between_till_flag = True
                    starttime = '00:00:01'
                enddate = str(datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                default_endtime = datetime.datetime.strptime(reschedule_timestamp['to'][11:19],'%H:%M:%S')
                endtime = str(default_endtime - timedelta(hours = 1))[11:19]
            elif startdate is None and enddate is not None:
                current = datetime.datetime.now(timezone)
                #if time_text.split()[0] == 'till':
                if 'till' in time_text.split():  #my change
                    startdate=str(datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                else:
                    startdate = str(current.date()) 
                # Checking if it is equal to today date and if today date -taking current time else taking 12 am
                if startdate == str(datetime.datetime.now(timezone).date()):
                    starttime = str(current.time())[:8]
                else:
                    starttime = '00:00:01'
                enddate = str(datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = str(datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            # Handling for between 
            elif startdate is not None and enddate is None:
                end = datetime.datetime.now(timezone)
                startdate = str(datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                starttime = str(datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                enddate = str(datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = "23:59:00"
            # Generating list of date from startdate and end date 
            mydates = pd.date_range(startdate, enddate).tolist() 
            #date = startdate
            # Getting dates
            for i in mydates:
                date_list.append(str(i.date()))
            print('date_list',date_list)
            # Checking for conditions to extract needed time of appointment
            booking_id = []
            if len(date_list) == 1:
                date_list = date_list
            else:
                date_list = date_list[:-1]
                date_multiple=date_list[-1]
                multiple_date = True
            # Passing dates form list
            headers = {'authorization':user_auth,'Content-Type': 'application/json'}
            params={'calendarType':'daily','doctorId':doctor_id,'isCount':'false'}
            url=BASE_URL+'appointment/filter'
            for date in date_list:
                body={
                "dates": [
                    date
                ]
                }
                request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
                print("status code of fileter_api_dct in reschedule",request.status_code)
                if request.status_code == 401:
                    output = static_responses.UNAUTHORISED_USER
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response) 
                get_booking_id = request.json()
                print("start_time",starttime)
                print("end_time",endtime)
                # Appending all the booking id to a list
                if starttime == '00:00:00':
                    try:
                        for i in range(len(get_booking_id['result'][0]['sessions'])):
                            for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                                if get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                    booking_id.append(['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                    except:
                        continue
                else:
                    try:
                        for i in range(len(get_booking_id['result'][0]['sessions'])):
                            for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                                if starttime <= get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and endtime >= get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                    booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                    except:
                        continue
        print('booking id',booking_id)
        # Checking the length of booking id
        if len(booking_id) == 0:
            if reschedule_timestamp['from'] is not None and reschedule_timestamp['to'] is not None:
                datetime_start = datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                datetime_end = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                if multiple_date is True:
                    todate=datetime.datetime.strptime(date_multiple,'%Y-%m-%d').strftime('%d %B')
                    output = "Doc, there are no appointments from {} to {}".format(datetime_start,todate)
                elif reschedule_timestamp['from'][:10] == reschedule_timestamp['to'][:10]:
                    output = "Doc, there are no appointments {}.".format(time_text)
                else:
                    output = "Doc, there are no appointments from {} to {}".format(datetime_start,datetime_end)
            elif reschedule_timestamp['from'] is not None and reschedule_timestamp['to'] is None:
                datetime_start_time = datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                datetime_start_date = datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                output =  "Doc, there are no appointments after {} on {}.".format(datetime_start_time,datetime_start_date)
            elif reschedule_timestamp['from'] is None and reschedule_timestamp['to'] is not None:
                datetime_end_time = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                datetime_end_date = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                output =  "Doc, there are  no appointments till {} on {}.".format(datetime_end_time,datetime_end_date)
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

        else:
            if len(booking_id) == 1:
                len_book = 'appointment'
            else: 
                len_book = 'appointments'
            # Response for between 
            if reschedule_timestamp['from'] is not None and reschedule_timestamp['to'] is not None:
                datetime_start = datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                datetime_end = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                start_time=datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                end_time=datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                if multiple_date is True:
                    date_multiple1=datetime.datetime.strptime(date_multiple,'%Y-%m-%d').strftime('%d %B')
                    output = "Doc, there are  {} ".format(len(booking_id)) +len_book+" from {} to {}".format(datetime_start,date_multiple1)+" "+"Should i proceed to reschedule the appointments?"
                elif between_till_flag is True:
                    #datetime_end = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                    default_endtime = datetime.datetime.strptime(reschedule_timestamp['to'][11:19],'%H:%M:%S')
                    datetime_end1 = str(default_endtime - timedelta(hours = 1))
                    datetime_end=datetime.datetime.strptime(datetime_end1[11:19],'%H:%M:%S').strftime('%I:%M %p')
                    date = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                    output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" "+"on {}".format(date)+" till {}.".format(datetime_end) +" "+ "Should i proceed to reschedule the appointments?"
                elif reschedule_timestamp['from'][:10] == reschedule_timestamp['to'][:10]:
                    output = "Doc, there are  {} ".format(len(booking_id)) +len_book+" on {} between {} to {}.".format(datetime_start,start_time,end_time)+" "+"Should i proceed to reschedule the appointments?"
                else:
                    output = "Doc, there are  {} ".format(len(booking_id)) +len_book+" from {} to {}.".format(datetime_start,datetime_end)+" "+"Should i proceed to reschedule the appointments?"
            # Response for after
            elif reschedule_timestamp['from'] is not None and reschedule_timestamp['to'] is None:
                datetime_start = datetime.datetime.strptime(reschedule_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" after {}.".format(datetime_start) +" "+ "Should i proceed to reschedule the appointments?"
            # Response for till
            elif reschedule_timestamp['from'] is None and reschedule_timestamp['to'] is not None:
                datetime_end = datetime.datetime.strptime(reschedule_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" till {}.".format(datetime_end) +" "+ "Should i proceed to reschedule the appointments?"
            session_attr["source"]='reschedule'
            session_attr["booking_id"]=booking_id
            print(output)
            return (handler_input.response_builder.speak(output).set_should_end_session(False).response)


class CancelAppointment(AbstractRequestHandler):
    """Handler for Cancel Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_intent_name("cancelappointment")(handler_input)

    def handle(self, handler_input):
        statuscode,doctor_id,user_auth=sheela_utils.get_fhb_info(handler_input)
        if statuscode == 201:
            output = static_responses.ACCOUNT_LINK
            return (handler_input.response_builder.speak(output).set_card(Card('LinkAccount')).set_should_end_session(True).response)
        elif statuscode == 202:
            output = static_responses.PROFILE_SETUP
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
        print("user_auth",user_auth,doctor_id)
        slots = handler_input.request_envelope.request.intent.slots
        session_attr = handler_input.attributes_manager.session_attributes        
        time_zone="Asia/Kolkata"
        time_text_value=slots['text']
        time_text=time_text_value.value
        between_till_flag = None
        multiple_date = None
        # Handling if cancel is redirected from querying
        try:
            source=session_attr.get("source")
            booking_id=session_attr.get("booking_id")
            if source == 'querying':
                session_attr["source"]='cancellation'
                session_attr["booking_id"]=booking_id
                return handler_input.response_builder.add_directive(StartConnectionDirective(uri='connection://AMAZON.VerifyPerson/1',input={})).response
        except:
            pass
        # Checking if doctor statement has date to cancel or using elicit slot get the date
        if time_text is None:
            output='please give the date'
            return (handler_input.response_builder.speak(output).set_should_end_session(False).add_directive(
                directive=ElicitSlotDirective( 
                    slot_to_elicit="text")).response)


        url=DUCKLING_URL
        payload = {"tz":time_zone,"text":time_text}
        output = requests.post(url, data=payload)
        duckling=output.json()
        cancel_timestamp = None
        try:
            cancel_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':duckling[0]['value']['values'][0]['from']['value']}
        except:
            pass
        if cancel_timestamp is None:
            try:
                cancel_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':None}
            except:
                try:
                    cancel_timestamp={'to':None,'from':duckling[0]['value']['values'][0]['from']['value']}
                except:
                    pass
        if cancel_timestamp is None:
            try:
                cancel_timestamp=duckling[0]['value']['values'][0]['value']
            except:
                pass
        print("cancel_timestamp",cancel_timestamp,type(cancel_timestamp))
        print("i am ok")

        if time_text is None:
            time_text='sankar'
        timezone = pytz.timezone('Asia/Kolkata')
        
        # Checking the type of timestamp
        if isinstance(cancel_timestamp, str):
            print("inside str")
            date = str(datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
            only_time = str(datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            # Fetching sessions with Doctor id
            #time_taken= time.time()
            print("doc",user_auth,doctor_id)
            headers = {'authorization': user_auth,'Content-Type': 'application/json'}
            params={'calendarType':'daily','doctorId':doctor_id,'isCount':'false'}
            url=BASE_URL+'appointment/filter'
            body={
            "dates": [
                date
            ]
            }
            request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
            print("status code of fileter_api_str in cancel",request.status_code)
            if request.status_code == 401:
                output = static_responses.UNAUTHORISED_USER
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response) 
            get_booking_id = request.json()
            # Appending all the booking id to a list
            booking_id=[]
            # Condition if only doctor gives the date with out time
            if only_time == '00:00:00':
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            # Checking if user text has from word in it
            elif 'from' in time_text.split():
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if only_time < get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            # Handiling if doctor has specified the time
            else:
                try:
                    for i in range(len(get_booking_id['result'][0]['sessions'])):
                        for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                            if only_time == get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                except:
                    pass
            if len(booking_id) == 0:
                #output = "Doc, there are no appointments on the mentioned date"
                if cancel_timestamp[11:19] == '00:00:00':
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta (days=1)
                    if cancel_timestamp[:10] == str(current):
                        output = static_responses.NO_APPOINTMENTS_TODAY
                    elif cancel_timestamp[:10] == str(tomorrow):
                        output = static_responses.NO_APPOINTMENTS_TOMORROW
                    elif cancel_timestamp[:10] == str(day_after_tomorrow):
                        output = static_responses.NO_APPOINTMENTS_DAY_AFTER_TOMORROW
                    else:
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            #output = "Doc, there are no appointments {}.".format(time_text)
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are no appointments on {}.".format(time_text)
                        # Returing the response with date of cancellation
                        else:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are no appointments on {}.".format(date_time)
                else:
                    # Checking if doctor specified date is today,tomorrow or day after tomorrow
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta(days=1)
                    date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                    if cancel_timestamp[:10] == str(current):
                        output = "Doc, there are no appointments today at {}".format(date_time)
                    elif cancel_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are no appointments tomorrow at {}.".format(date_time)
                    elif cancel_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are no appointments day after tomorrow at {}.".format(date_time)
                    else:
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are no appointments on {}.".format(time_text)
                        # Returing the response with date of cancellation
                        else:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are no appointments on {}. ".format(date_time)
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

            else:
                print('booking id',booking_id)
                session_attr["booking_id"] = booking_id
                session_attr["source"]='cancellation'
                if cancel_timestamp[11:19] == '00:00:00':
                    if len(booking_id) == 1:
                        len_book = 'appointment'
                    else: 
                        len_book = 'appointments'
                    # Checking if doctor specified date is today,tomorrow or day after tomorrow
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta (days=1)
                    if cancel_timestamp[:10] == str(current):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"today. Should i proceed to cancel the appointments?"
                    elif cancel_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book +" "+"tomorrow. Should i proceed to cancel the appointments?"
                    elif cancel_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are {}".format(len(booking_id))+" "+len_book +" "+"day after tomorrow. Should i proceed to cancel the appointments?"
                    else:
                        date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                        #output = "Doc, there are {} appointments on {}. Should i proceed to cancel the appointments?".format(len(booking_id),date_time)
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            output ="Doc, there are {}".format(len(booking_id))+" "+" "+len_book+" "+"{}. Should i proceed to cancel the appointments?".format(time_text)
                        # Returing the response with date of cancellation
                        else:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"on {}. Should i proceed to cancel the appointments?".format(date_time)
                    print(output)
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
                else:
                    if len(booking_id) == 1:
                        len_book = 'appointment'
                    else: 
                        len_book = 'appointments'
                    # Checking if doctor specified date is today,tomorrow or day after tomorrow
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta(days=1)
                    date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                    if cancel_timestamp[:10] == str(current):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"today at {}.".format(date_time)+"Should I proceed to cancel the appointments?"
                    elif cancel_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"tomorrow at {}.".format(date_time)+"Should I proceed to cancel the appointments?"
                    elif cancel_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are {}".format(len(booking_id)) +" "+ len_book+" "+"day after tomorrow at {}.".format(date_time)+"Should I proceed to cancel the appointments?"
                    else:
                        # If user text has next,upcoming and coming in text , response according to it or send the date in response
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration or time_text.split()[1] in duration:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                            output = "Doc, there are {}".format(len(booking_id))+" "+len_book+" "+"{} at {}. Should i proceed to cancel the appointments?".format(len(booking_id),time_text,date_time)
                         # Returing the response with date and time specified of cancellation
                        else:
                            date_time = datetime.datetime.strptime(cancel_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are {}".format(len(booking_id)) + " "+len_book +" "+" on {}.".format(date_time) + "Should I proceed to cancel the appointments?"
                    print(output)
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).response)

        elif isinstance (cancel_timestamp, dict): 
            # Checking the type of timestamp
            # Fetching sessions with Doctor id
            date_list=[]
            startdate = cancel_timestamp['from']
            starttime = cancel_timestamp['from']
            enddate = cancel_timestamp['to']
            endtime = cancel_timestamp['to']
            # Checking condition with the timestamp from slots and assigning a variable for future usage
            # Handling for between 
            if startdate is not None and enddate is not None:
                startdate = str(datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                starttime = str(datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                if starttime == "00:00:00":
                    between_till_flag = True
                    starttime = '00:00:01'
                enddate = str(datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = str(datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            # Handling for till
            elif startdate is None and enddate is not None:
                current = datetime.datetime.now(timezone)
                #if time_text.split()[0] == 'till':
                if 'till' in time_text.split():
                    startdate=str(datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                else:
                    startdate = str(current.date()) 
                # If date is current date taking present time
                if startdate == str(datetime.datetime.now(timezone).date()):
                    starttime = str(current.time())[:8]
                else:
                    starttime = '00:00:01'
                enddate = str(datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = str(datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            # Handling for after
            elif startdate is not None and enddate is None:
                end = datetime.datetime.now(timezone)
                startdate = str(datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                starttime = str(datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                enddate = str(datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = "23:59:00"
            mydates = pd.date_range(startdate, enddate).tolist() 
            print('mydates',mydates)
            #date = startdate
            for i in mydates:
                date_list.append(str(i.date()))
            # Checking for conditions to extract needed time of appointment
            booking_id = []
            if len(date_list) == 1:
                date_list = date_list
            else:
                multiple_date = True
                date_list = date_list[:-1]
                date_multiple=date_list[-1]
            print('date_list',date_list)
            headers = {'authorization':user_auth,'Content-Type': 'application/json'}
            params={'calendarType':'daily','doctorId':doctor_id,'isCount':'false'}
            url=BASE_URL+'appointment/filter'
            for date in date_list:
                body={
                "dates": [
                    date
                ]
                }
                request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
                print("status code of fileter_api_dct in cancel",request.status_code)
                if request.status_code == 401:
                    output = static_responses.UNAUTHORISED_USER
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)  
                get_booking_id = request.json()
                # Appending all the booking id to a list
                # Handling if user has not specified time
                if starttime == '00:00:00':
                    try:
                        for i in range(len(get_booking_id['result'][0]['sessions'])):
                            for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                                if get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                    booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                    except:
                        continue
                else:
                    try:
                        for i in range(len(get_booking_id['result'][0]['sessions'])):
                            for j in range(len(get_booking_id['result'][0]['sessions'][i]['appointments'])):
                                if starttime <= get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and endtime >= get_booking_id['result'][0]['sessions'][i]['appointments'][j]['plannedStartDateTime'][11:19] and get_booking_id['result'][0]['sessions'][i]['appointments'][j]['status']['description'] =='Booked':
                                    booking_id.append(get_booking_id['result'][0]['sessions'][i]['appointments'][j]['bookingId'])
                    except:
                        continue
        print('booking id',booking_id)
        if len(booking_id) == 0:
            if cancel_timestamp['from'] is not None and cancel_timestamp['to'] is not None:
                datetime_start = datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                datetime_end = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                betweentime1=datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                betweentime2=datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                if multiple_date is True:
                    todate=datetime.datetime.strptime(date_multiple,'%Y-%m-%d').strftime('%d %B')
                    output = "Doc, there are no appointments from {} to {}".format(datetime_start,todate)
                elif cancel_timestamp['from'][:10] == cancel_timestamp['to'][:10]:
                    output = "Doc, there are no appointments {} between {} and {}.".format(datetime_start,betweentime1,betweentime2)
                else:
                    output = "Doc, there are no appointments from {} to {}".format(datetime_start,datetime_end)
            elif cancel_timestamp['from'] is not None and cancel_timestamp['to'] is None:
                datetime_start_time = datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                datetime_start_date = datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                output =  "Doc, there are no appointments after {} on {}.".format(datetime_start_time,datetime_start_date)
            elif cancel_timestamp['from'] is None and cancel_timestamp['to'] is not None:
                datetime_end_time = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                datetime_end_date = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                output =  "Doc, there are  no appointments till {} on {}.".format(datetime_end_time,datetime_end_date)
            print("reponse of cancel dict",output)
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
        else:
            if len(booking_id) == 1:
                len_book = 'appointment'
            else: 
                len_book = 'appointments'
            # Response for between
            if cancel_timestamp['from'] is not None and cancel_timestamp['to'] is not None:
                datetime_start = datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                datetime_end = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                betweentime1=datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                betweentime2=datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I:%M %p')
                if multiple_date is True:
                    date_multiple1=datetime.datetime.strptime(date_multiple,'%Y-%m-%d').strftime('%d %B')
                    output = "Doc, there are  {} ".format(len(booking_id)) +len_book+" from {} to {}".format(datetime_start,date_multiple1)+" "+"Should I proceed to cancel the appointments?"

                elif  between_till_flag is True:
                    datetime_end = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                    output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" till {}".format(datetime_end) +" "+ "Should I proceed to cancel the appointments?"
                elif cancel_timestamp['from'][:10] == cancel_timestamp['to'][:10]:
                    starttime =  datetime.datetime.strptime(
                        starttime, "%H:%M:%S"
                    ).strftime("%I:%M %p")
                    endtime = datetime.datetime.strptime(
                        endtime, "%H:%M:%S"
                    ).strftime("%I:%M %p")
                    output = (
                        "Doc, there are {} ".format(len(booking_id))
                        + len_book
                        + " on {} between {} to {}.".format(datetime_start,starttime,endtime)
                        + " "
                        + "Should I proceed to cancel the appointments?"
                    )
                else:
                    output = "Doc, there are  {} ".format(len(booking_id)) +len_book+" from {} to {}".format(datetime_start,datetime_end)+" "+"Should I proceed to cancel the appointments?"
             # Response for after
            elif cancel_timestamp['from'] is not None and cancel_timestamp['to'] is None:
                datetime_start = datetime.datetime.strptime(cancel_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" after {}".format(datetime_start) +" "+ "Should I proceed to cancel the appointments?"
             # Response for till
            elif cancel_timestamp['from'] is None and cancel_timestamp['to'] is not None:
                datetime_end = datetime.datetime.strptime(cancel_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                output =  "Doc, there are  {} ".format(len(booking_id)) +len_book+" till {}".format(datetime_end) +" "+ "Should I proceed to cancel the appointments?"
            # Sending booking_id and source to session attributes for further use
            session_attr["booking_id"] = booking_id
            session_attr["source"]='cancellation'
            print(output)
            return (handler_input.response_builder.speak(output).set_should_end_session(False).response)

class Appointmentqry(AbstractRequestHandler):
    """Handler for Query Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_intent_name("appointmentqry")(handler_input)
    

    def handle(self, handler_input): 
        timezone = pytz.timezone('Asia/Kolkata')
        statuscode,doctor_id,user_auth=sheela_utils.get_fhb_info(handler_input)
        if statuscode == 201:
            output = static_responses.ACCOUNT_LINK
            return (handler_input.response_builder.speak(output).set_card(Card('LinkAccount')).set_should_end_session(True).response)
        elif statuscode == 202:
            output = static_responses.PROFILE_SETUP
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
        dict_cond = None
        till_duration = None
        till_duration_today = None
        till_duration1 = None
        between_flag = None
        from_flag = None
        list_of_text=[]
        slots = handler_input.request_envelope.request.intent.slots
        session_attr = handler_input.attributes_manager.session_attributes  
        time_text_value=slots['text']
        time_text=time_text_value.value      
        time_zone="Asia/Kolkata"
        time_text_value=slots['text']
        time_text=time_text_value.value
        url=DUCKLING_URL
        payload = {"tz":time_zone,"text":time_text}
        output = requests.post(url, data=payload)
        try:
            duckling=output.json()
        except:
            pass
        query_timestamp = None
        try:
            query_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':duckling[0]['value']['values'][0]['from']['value']}
        except:
            pass
        if query_timestamp is None:
            try:
                query_timestamp={'to':duckling[0]['value']['values'][0]['to']['value'],'from':None}
            except:
                try:
                    query_timestamp={'to':None,'from':duckling[0]['value']['values'][0]['from']['value']}
                except:
                    pass
        if query_timestamp is None:
            try:
                query_timestamp=duckling[0]['value']['values'][0]['value']
            except:
                pass
        if query_timestamp is None:      #if time is not mentioned suring querying, current date is set as reschedule timestamp
            query_timestamp = str(datetime.datetime.now(timezone).date())+'T00:00:00.000+05:30'
        time_text_value=slots['text']
        time_text=time_text_value.value
        if time_text is None:
            time_text='sankar'
        timezone = pytz.timezone('Asia/Kolkata')
        

        if isinstance(query_timestamp, str):
            # Handling if user didn't specify time
            if query_timestamp[11:19] == "00:00:00":
                # Checking if user date is current date , If so taking the current time
                if query_timestamp[:10] == str(datetime.datetime.now(timezone).date()):
                    no_time = True
                    starttime1= str(datetime.datetime.now(timezone).time())
                    starttime = starttime1[:8]
                    endtime = "23:59:59"
                else:
                    no_time = True
                    starttime = "00:00:00"
                    endtime = "23:59:59"
            else:
                if 'from' in time_text.split():
                    from_flag = True
                no_time = False
                starttime = query_timestamp[11:19]
            dict_cond = False
            headers = {"authorization": 'Bearer'+ ' ' + user_auth, "Content-Type": "application/json"}
            url = BASE_URL + "doctor/"+doctor_id+"?include=all"
            request = requests.get(url, headers=headers)

            print("status code of session_api_str in query",request.status_code)
            if request.status_code == 401:
                output = static_responses.UNAUTHORISED_USER
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response) 
            date = str(datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
            try:
                session_list=request.json()
                ses_id=[]
                session_times=[]
                session_day1=pd.to_datetime(query_timestamp)
                session_day = session_day1.strftime('%A')
                #date = str(datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                only_time = str(datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                # getting session and session start and end time based on effective date and is available
                for i in range(len(session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'])):
                    ses_id.append(session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['id'])
                    session_times.append(
                        {
                            "id": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['id'],
                            "session_start_time": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['startTime'],
                            "session_end_time": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['endTime']
                        })


                # This dictionry is used below to map the start and end time of the session
                newdata = {}
                for entry in session_times:
                    name = entry.pop('id') #remove and return the name field to use as a key
                    newdata[name] = entry
                print("new_data inside try",newdata)
            except:
                session_list = []
                print("except in get_session")
                pass  
            booking_id = []
            planned_start_time=[]
            planned_end_time=[] 
            booking_id_count=[]
            new_ses_id = None
            headers = {'authorization': user_auth,'Content-Type': 'application/json'}
            params={'calendarType':'daily','doctorId':doctor_id,'isCount':'false'}
            url=BASE_URL+'appointment/filter'
            body={
            "dates": [
                date
            ]
            }
            request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
            print("status code of filter_api_str in query",request.status_code)
            if request.status_code == 401:
                output = static_responses.UNAUTHORISED_USER
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
            new_ses_id = i
            get_reschedule_details = request.json()
            # Getting booking_id and start and end time of the booking id and checking status if it is booked.
            try:
                session_id = []
                for i in range(
                    len(get_reschedule_details['result'][0]['sessions'])
                ):
                    for j in range(
                        len(
                            get_reschedule_details['result'][0]['sessions'][i]['appointments']
                        )
                    ):
                        # Check whether ths appointment status is booked
                        if (
                            get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]['status']['description']
                            == "Booked"
                        ):
                            # booking_id_count.append({'id': get_reschedule_details['response']['data'][0]['sessions'][i]['sessionId'] ,'count':len(get_reschedule_details['response']['data'][0]['sessions'][i]['appointments'])})
                            booking_id.append(
                                {
                                    "booking_id": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]['bookingId'],
                                    "start_time": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]["plannedStartDateTime"][11:19],
                                    "end_time": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]["plannedEndDateTime"][11:19],
                                    "session_id": get_reschedule_details['result'][0]['sessions'][i]['sessionId']
                                }
                            )
                for i in range(len(booking_id)):  # To get the count of each session
                    booking_id_count.append(
                        {
                            "id": booking_id[i]["session_id"],
                            "count": sum(
                                x.get("session_id") == booking_id[i]["session_id"]
                                for x in booking_id
                            ),
                        }
                    )
                booking_id_count = [
                    dict(t) for t in {tuple(d.items()) for d in booking_id_count}
                ]  # Removing the duplicate sessions
                print('booking id count',booking_id_count)
            except:
                pass

        elif isinstance(query_timestamp, dict):
            print("inside dict")
            date_list =[]
            if query_timestamp['from'] is not None and query_timestamp['to'] is not None:
                #Enabling between flag for response handling
                between_flag = True
                print("inside between")
                startdate = str(datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date()) 
                starttime = str(datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                enddate = str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                #string_time=str(default_endtime-timedelta(hours=1))
                #endtime = str(datetime.datetime.strptime(string_time,'%Y-%m-%d %H:%M:%S').time())
            # Handling till
            elif query_timestamp['from'] is None and  query_timestamp['to'] is not None:
                #Enabling between till flag for response handling
                till_duration1 = True
                print("inside till")
                current = datetime.datetime.now(timezone)
                if 'till' in time_text.split():
                    startdate=str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                else:
                    startdate = str(current.date())                                                 #my change
                #startdate = str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                # if date is current date , taking current time
                if startdate == str(datetime.datetime.now(timezone).date()):
                    starttime = str(current.time())[:8]
                    till_duration_today = True
                else:
                    starttime = '00:00:00'
                    till_duration = True
                enddate = str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = str(datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
            elif query_timestamp['from'] is not None and  query_timestamp['to'] is None:
                # Enabling flag for response handling
                dict_cond = True
                end = datetime.datetime.now(timezone)
                startdate = str(datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date()) 
                starttime = str(datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').time())
                enddate = str(datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').date())
                endtime = "23:59:00" 
            mydates = pd.date_range(startdate, enddate).tolist() 
            #date = startdate
            for i in mydates:
                date_list.append(str(i.date()))
            # To restrict doctors from querying multiple dates
            if len(date_list) > 1: # restricting doctor for multiple dates
                output =static_responses.QUERY_FOR_MULTIPLE_DATE
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
            else:
                headers = {"authorization": 'Bearer'+ ' ' + user_auth, "Content-Type": "application/json"}
                url = BASE_URL + "doctor/"+doctor_id+"?include=all"
                request = requests.get(url, headers=headers)
                print("status code of session_api_dct in query",request.status_code)
                if request.status_code == 401:
                    output = static_responses.UNAUTHORISED_USER
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
                try:                       
                    session_list=request.json()
                except:
                    session_list = []
                    pass
                ses_id=[]
                session_times=[]
                #session_day = session_day1.strftime('%A')
                date_list=[] 
                # Handling between
                
                for i in range(len(session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'])):
                    ses_id.append(session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['id'])
                    session_times.append(
                        {
                            "id": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['id'],
                            "session_start_time": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['startTime'],
                            "session_end_time": session_list["result"]['providerMappingCollection'][0]['doctorSessionCollection'][i]['endTime']
                        })
                newdata = {}
                for entry in session_times:
                    name = entry.pop('id') #remove and return the name field to use as a key
                    newdata[name] = entry
                booking_id = []
                planned_start_time=[]
                planned_end_time=[] 
                booking_id_count=[]
                new_ses_id = None
                headers = {'authorization': user_auth,'Content-Type': 'application/json'}
                params={'calendarType':'daily','doctorId': doctor_id,'isCount':'false'}
                url=BASE_URL+'appointment/filter'
                body={
                "dates": [
                    startdate
                ]
                }
                request=requests.post(url,headers=headers,params=params,data=json.dumps(body))
                print("status code of filter_api_dct in query",request.status_code)
                if request.status_code == 401:
                    output = static_responses.UNAUTHORISED_USER
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)  
                #new_ses_id = i
                get_reschedule_details = request.json()
                try:
                    for i in range(
                        len(get_reschedule_details['result'][0]['sessions'])
                    ):
                        for j in range(
                            len(
                                get_reschedule_details['result'][0]['sessions'][i]['appointments']
                            )
                        ):
                            if (
                                get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]['status']['description']
                                == "Booked"
                            ):
                                # booking_id_count.append({'id': get_reschedule_details['response']['data'][0]['sessions'][i]['sessionId'] ,'count':len(get_reschedule_details['response']['data'][0]['sessions'][i]['appointments'])})
                                booking_id.append(
                                    {
                                        "booking_id": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]['bookingId'],
                                        "start_time": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]["plannedStartDateTime"][11:19],
                                        "end_time": get_reschedule_details['result'][0]['sessions'][i]['appointments'][j]["plannedEndDateTime"][11:19],
                                        "session_id": get_reschedule_details['result'][0]['sessions'][i]['sessionId']
                                    }
                                )
                    for i in range(len(booking_id)):
                        booking_id_count.append(
                            {
                                "id": booking_id[i]["session_id"],
                                "count": sum(
                                    x.get("session_id") == booking_id[i]["session_id"]
                                    for x in booking_id
                                ),
                            }
                        )
                    booking_id_count = [
                        dict(t) for t in {tuple(d.items()) for d in booking_id_count}
                    ]  # Removing the duplicate sessions
                except:
                    pass

        print('booking_id',booking_id)
        print('new_data',newdata)
        output = []
        common_var = "Doc, you have "
        if len(booking_id_count) == 0:
            if isinstance(query_timestamp,dict):
                if query_timestamp['from'] is not None and query_timestamp['to'] is not None:
                    datetime_start = datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                    datetime_end = datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                    if query_timestamp['from'][:10] == query_timestamp['to'][:10]:
                        output = "Doc, there are no appointments {}.".format(time_text)
                    else:
                        output = "Doc, there are no appointments from {} to {}".format(datetime_start,datetime_end)
                elif query_timestamp['from'] is not None and query_timestamp['to'] is None:
                    datetime_start = datetime.datetime.strptime(query_timestamp['from'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                    output =  "Doc, there are no appointments after {}.".format(datetime_start)
                elif query_timestamp['from'] is None and query_timestamp['to'] is not None:
                    datetime_end = datetime.datetime.strptime(query_timestamp['to'],'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I:%M %p')
                    output =  "Doc, there are  no appointments till {}.".format(datetime_end)
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

            elif isinstance(query_timestamp,str):
                if query_timestamp[11:19] == '00:00:00':
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta (days=1)
                    if query_timestamp[:10] == str(current):
                        output = static_responses.NO_APPOINTMENTS_TODAY
                    elif query_timestamp[:10] == str(tomorrow):
                        output = static_responses.NO_APPOINTMENTS_TOMORROW
                    elif query_timestamp[:10] == str(day_after_tomorrow):
                        output = static_responses.NO_APPOINTMENTS_DAY_AFTER_TOMORROW
                    else:
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration:
                            output = "Doc, there are no appointments {}.".format(time_text)
                        else:
                            date_time = datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B')
                            output = "Doc, there are no appointments on {}.".format(date_time)
                else:
                    current = datetime.datetime.now(timezone).date()
                    tomorrow = current + datetime.timedelta(days=1)
                    day_after_tomorrow = tomorrow + datetime.timedelta(days=1)
                    date_time = datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                    if query_timestamp[:10] == str(current):
                        output = "Doc, there are no appointments today at {}".format(date_time)
                    elif query_timestamp[:10] == str(tomorrow):
                        output = "Doc, there are no appointments tomorrow at {}.".format(date_time)
                    elif query_timestamp[:10] == str(day_after_tomorrow):
                        output = "Doc, there are no appointments day after tomorrow at {}.".format(date_time)
                    else:
                        duration =['next','upcoming','coming']
                        if time_text.split()[0] in duration:
                            date_time = datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%I %M %p')
                            output = "Doc, there are no appointments {} at {}.".format(time_text,date_time)
                        else:
                            date_time = datetime.datetime.strptime(query_timestamp,'%Y-%m-%dT%H:%M:%S.%f+05:30').strftime('%d %B %I %M %p')
                            output = "Doc, there are no appointments on {}. ".format(date_time)
                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
                

        else:
            booking_id_slots =[]
            final_book=[]
            session_id=[]
            # Handling based on time ranges
            if booking_id_count[0]['count'] == 1:
                booking_count = 'appointment'
            else:
                booking_count = 'appointments'
            if dict_cond is True:
                for i in range(len(booking_id)):
                    start_time = datetime.datetime.strptime(starttime,'%H:%M:%S')
                    end_time1 = booking_id[i]['start_time']
                    end_time = datetime.datetime.strptime(end_time1,'%H:%M:%S')
                    diff = start_time - end_time
                    if diff.total_seconds() <= 0:
                        session_id.append(booking_id[i]['session_id'])
                    else:
                        pass
                session_id = list(set(session_id))
                for j in session_id:
                    for i in range(len(booking_id)):
                        if j == booking_id[i]['session_id']:
                            booking_id_slots.append(booking_id[i]['booking_id'])
                booking_id_slots = list(set(booking_id_slots))
                session_attr["booking_id"] = booking_id_slots
                session_attr["source"] = "querying"
                for i in range(len(booking_id_count)):
                    if booking_id_count[i]['id'] in session_id:
                        final_book.append({'id':booking_id_count[i]['id'],'count':booking_id_count[i]['count']})
                for i in range(len(final_book)):
                    ids=final_book[i]['id']
                    start1 = newdata[ids]['session_start_time']
                    start = datetime.datetime.strptime(start1, "%H:%M:%S").strftime("%I:%M %p")
                    end1 = newdata[ids]['session_end_time']
                    end = datetime.datetime.strptime(end1, "%H:%M:%S").strftime("%I:%M %p")
                    output.append(str(final_book[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start + " "+ "to" +" "+ end+",")
                print("output",output)    
                if len(output) == 0:
                    output ="Doc, You dont have any appointment on the mentioned time"
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
                    
            elif till_duration1 is True:
                # Comparing whether session starttime is greater than doctor's given time
                if till_duration_today is True:
                    ids=booking_id_count[i]['id']
                    for i in range(len(booking_id)):
                        start_time = datetime.datetime.strptime(starttime,'%H:%M:%S')
                        end_time1 = booking_id[i]['start_time']
                        end_time = datetime.datetime.strptime(end_time1,'%H:%M:%S')
                        diff = start_time - end_time
                        if diff.total_seconds() >= 0:
                            session_id.append(booking_id[i]['session_id'])
                        else:
                            pass
                    session_id = list(set(session_id))
                    for j in session_id:
                        for i in range(len(booking_id)):
                            if j == booking_id[i]['session_id']:
                                booking_id_slots.append(booking_id[i]['booking_id'])
                    booking_id_slots = list(set(booking_id_slots))
                    session_attr["booking_id"] = booking_id_slots
                    session_attr["source"] = "querying"
                    for i in range(len(booking_id_count)):
                        if booking_id_count[i]['id'] in session_id:
                            final_book.append({'id':booking_id_count[i]['id'],'count':booking_id_count[i]['count']})
                    for i in range(len(final_book)):
                        ids=final_book[i]['id']
                        start1 = newdata[ids]['session_start_time']
                        start = datetime.datetime.strptime(start1, "%H:%M:%S").strftime("%I:%M %p")
                        end1 = newdata[ids]['session_end_time']
                        end = datetime.datetime.strptime(end1, "%H:%M:%S").strftime("%I:%M %p")
                        output.append(str(final_book[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start  + " "+ "to" +" "+ end +",")
                
                else:
                    for i in range(len(booking_id)):
                        start_time = datetime.datetime.strptime(endtime,'%H:%M:%S')
                        end_time1 = booking_id[i]['start_time']
                        end_time = datetime.datetime.strptime(end_time1,'%H:%M:%S')
                        diff = start_time - end_time
                        if diff.total_seconds() >= 0:
                            session_id.append(booking_id[i]['session_id'])
                        else:
                            pass
                    session_id = list(set(session_id))
                    for j in session_id:
                        for i in range(len(booking_id)):
                            if j == booking_id[i]['session_id']:
                                booking_id_slots.append(booking_id[i]['booking_id'])
                    booking_id_slots = list(set(booking_id_slots))
                    session_attr["booking_id"] = booking_id_slots
                    session_attr["source"] = "querying"
                    for i in range(len(booking_id_count)):
                        if booking_id_count[i]['id'] in session_id:
                            final_book.append({'id':booking_id_count[i]['id'],'count':booking_id_count[i]['count']})
                    final_book = [dict(t) for t in {tuple(d.items()) for d in final_book}]
                    for i in range(len(final_book)):
                        ids=final_book[i]['id']
                        start1 = newdata[ids]['session_start_time']
                        start = datetime.datetime.strptime(start1, "%H:%M:%S").strftime("%I:%M %p")
                        end1 = newdata[ids]['session_end_time']
                        end = datetime.datetime.strptime(end1, "%H:%M:%S").strftime("%I:%M %p")
                        output.append(str(final_book[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start+ " "+ "to" +" "+ end+",")
                if len(output) == 0:
                    output ="Doc, You dont have any appointment on the mentioned time"
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
            
            elif between_flag is True:
                for i in range(len(booking_id)):
                    start_time = datetime.datetime.strptime(starttime,'%H:%M:%S')
                    end_time = datetime.datetime.strptime(endtime,'%H:%M:%S')
                    book_start1 = booking_id[i]['start_time']
                    book_start = datetime.datetime.strptime(book_start1,'%H:%M:%S')
                    book_end1 = booking_id[i]['start_time']
                    book_end = datetime.datetime.strptime(book_end1,'%H:%M:%S')
                    diff_start = start_time - book_start
                    diff_end = end_time - book_end
                    if diff_start.total_seconds() <= 0 and diff_end.total_seconds() >= 0: 
                        session_id.append(booking_id[i]['session_id'])
                    else:
                        pass
                session_id = list(set(session_id))
                for j in session_id:
                    for i in range(len(booking_id)):
                        if j == booking_id[i]['session_id']:
                            booking_id_slots.append(booking_id[i]['booking_id'])
                booking_id_slots = list(set(booking_id_slots))
                session_attr["booking_id"] = booking_id_slots
                session_attr["source"] = "querying"
                for i in range(len(booking_id_count)):
                    if booking_id_count[i]['id'] in session_id:
                        final_book.append({'id':booking_id_count[i]['id'],'count':booking_id_count[i]['count']})
                for i in range(len(final_book)):
                    ids=final_book[i]['id']
                    start1 = newdata[ids]['session_start_time']
                    start = datetime.datetime.strptime(start1, "%H:%M:%S").strftime("%I:%M %p")
                    end1 = newdata[ids]['session_end_time']
                    end = datetime.datetime.strptime(end1, "%H:%M:%S").strftime("%I:%M %p")
                    output.append(str(final_book[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start+ " "+ "to" +" "+ end+",")
                    print("output",output)

                
                if len(output) == 0:
                    output ="Doc, You dont have any appointment on the mentioned time"
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

            #Handling for time string
            else:
                for i in range(len(booking_id_count)):
                    ids=booking_id_count[i]['id']
                    if no_time is True:
                        if newdata[ids]['session_start_time'] >= starttime and newdata[ids]['session_start_time'] <= endtime:
                            print('inside if')
                            for j in range(len(booking_id)):
                                if booking_id[j]['start_time'] >= starttime and booking_id[j]['start_time'] <= endtime:
                                    booking_id_slots.append(booking_id[j]['booking_id'])
                            booking_id_slots = list(set(booking_id_slots))
                            session_attr["booking_id"] = booking_id_slots
                            session_attr["source"] = "querying"
                            start1 = newdata[ids]['session_start_time']
                            end1 = newdata[ids]['session_end_time']
                            start = datetime.datetime.strptime(
                                    start1, "%H:%M:%S"
                                ).strftime("%I:%M %p")
                            end = datetime.datetime.strptime(
                                    end1, "%H:%M:%S"
                                ).strftime("%I:%M %p")
                            output.append(str(booking_id_count[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start+ " "+ "to" +" "+ end +",")
                            print(output)
                            
                        
                    elif no_time is False:
                        if from_flag is None:
                            for j in range(len(booking_id)):
                                if booking_id[j]['start_time'] == starttime:
                                    booking_id_slots.append(booking_id[j]['booking_id'])
                            booking_id_slots = list(set(booking_id_slots))
                            session_attr["booking_id"] = booking_id_slots
                            session_attr["source"] = "querying"
                            if len(booking_id_slots) == 1:
                                starttime = datetime.datetime.strptime(starttime,'%H:%M:%S').strftime('%I %M %p')
                                startdate1 = datetime.datetime.strptime(query_timestamp[:19],'%Y-%m-%dT%H:%M:%S').strftime('%d %B')
                                output ="Yes Doc, You have an appointment on {} at {},Would you like to cancel or reschedule ?".format(startdate1,starttime)
                                return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
                            else:
                                starttime = datetime.datetime.strptime(starttime,'%H:%M:%S').strftime('%I %M %p')
                                startdate1 = datetime.datetime.strptime(query_timestamp[:19],'%Y-%m-%dT%H:%M:%S').strftime('%d %B')
                                output = "Doc, you have no appointment on {} at {}".format (startdate1,starttime)
                                return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
                        else:
                            if 'from' in time_text.split(): # when user says on august {date} from {time}
                                ids=booking_id_count[i]['id']
                                for i in range(len(booking_id)):
                                    start_time = datetime.datetime.strptime(starttime,'%H:%M:%S')
                                    end_time1 = booking_id[i]['start_time']
                                    end_time = datetime.datetime.strptime(end_time1,'%H:%M:%S')
                                    diff = start_time - end_time
                                    if diff.total_seconds() < 0:
                                        session_id.append(booking_id[i]['session_id'])
                                    else:
                                        pass
                                session_id = list(set(session_id))
                                for j in session_id:
                                    for i in range(len(booking_id)):
                                        if j == booking_id[i]['session_id']:
                                            booking_id_slots.append(booking_id[i]['booking_id'])
                                booking_id_slots = list(set(booking_id_slots))
                                session_attr["booking_id"] = booking_id_slots
                                session_attr["source"] = "querying"
                                for i in range(len(booking_id_count)):
                                    if booking_id_count[i]['id'] in session_id:
                                        final_book.append({'id':booking_id_count[i]['id'],'count':booking_id_count[i]['count']})
                                for i in range(len(final_book)):
                                    ids=final_book[i]['id']
                                    start1 = newdata[ids]['session_start_time']
                                    start = datetime.datetime.strptime(start1, "%H:%M:%S").strftime("%I:%M %p")
                                    end1 = newdata[ids]['session_end_time']
                                    end = datetime.datetime.strptime(end1, "%H:%M:%S").strftime("%I:%M %p")
                                    output.append(str(final_book[i]['count']) +" "+ booking_count +" "+ "from" +" "+ start+ " "+ "to" +" "+ end+",")
                if len(output) == 0:
                    output ="Doc, You dont have any appointment on the mentioned time"
                    return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
            list_join = ' '.join(map(str,output))
            output = common_var +" "+ list_join + "Would you like to cancel or reschedule?"
            return (handler_input.response_builder.speak(output).set_should_end_session(False).response)

class YesIntentHandler(AbstractRequestHandler):
    """Handler for Yes Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.YesIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        session_attr = handler_input.attributes_manager.session_attributes
        if "booking_id" in session_attr and "source" in session_attr: # user affirm for cancel or reschedule
            source=session_attr.get("source")
            if source == 'cancellation':
                return handler_input.response_builder.add_directive(StartConnectionDirective(uri='connection://AMAZON.VerifyPerson/1',input={})).response
            elif source == "reschedule":
                return handler_input.response_builder.add_directive(StartConnectionDirective(uri='connection://AMAZON.VerifyPerson/1',input={})).response
            else:
                output = static_responses.YES_INTENT
                return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
        else:
            speak_output = static_responses.YES_INTENT

            return (handler_input.response_builder.speak(speak_output).set_should_end_session(False).response)
            
class SessionResumedRequestHandler(AbstractRequestHandler):
    """Handler for ResumeRequestHandler"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
       
        return ask_utils.is_request_type("SessionResumedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        cv=handler_input.request_envelope.request.cause.result
        if cv is None:
            output = static_responses.ACCOUNT_LINK

            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)

        #print("paylod_resumed",cv)
        status=cv['status']
        print("status",status)
        session_attr = handler_input.attributes_manager.session_attributes
        statuscode,doctor_id,user_auth=sheela_utils.get_fhb_info(handler_input)
        if statuscode == 201:
            output = static_responses.ACCOUNT_LINK
            return (handler_input.response_builder.speak(output).set_card(Card('LinkAccount')).set_should_end_session(True).response)
        elif statuscode == 202:
            output = static_responses.PROFILE_SETUP
            return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
        if status == 'VERIFIED': # Cancelling or reschedule based on verified message from profile match
            if "booking_id" in session_attr and "source" in session_attr:
                source=session_attr.get("source")
                if source == 'cancellation':
                    booking_id = session_attr.get("booking_id")
                    headers = {'authorization': user_auth ,'Content-Type': 'application/json'}
                    url=BASE_URL+'appointment'
                    body={
                    "cancellationSource": "doctor",
                    "bookingIds": 
                        booking_id
                    }
                    request = requests.put(url,headers=headers,data=json.dumps(body))
                    if request.status_code == 200:
                        print("response code of cancellation_api",request.status_code)
                        session_attr["source"] = "None"
                        output=static_responses.CANCEL_NOTIFICATION
                    else:
                        session_attr["source"] = "None"
                        print("response code of cancellation_api",request.status_code)
                        output = static_responses.INTERNAL_SERVER_ERROR
                    print("response of cancellation",output)
                    return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
                elif source == "reschedule":
                        booking_id = session_attr.get("booking_id")
                        reschedule_date = session_attr.get("reschedule_date")
                        header = {'authorization': user_auth,'Content-Type': 'application/json'}
                        url=BASE_URL+'appointment/reschedule'
                        body={
                            "bookingIds": 
                                booking_id,
                            "rescheduledDate": reschedule_date,
                            "rescheduleSource": "doctor"
                            }
                        request = requests.put(url,headers=header,data=json.dumps(body))
                        print("response code of cancellation_api",request.status_code)
                        if request.status_code == 200:
                            session_attr["source"] = "None"
                            output=static_responses.RESCHEDULE_NOTIFICATION
                        else:
                            session_attr["source"] = "None"
                            output=static_responses.INTERNAL_SERVER_ERROR
                        print("response of cancellation",output)
                        return (handler_input.response_builder.speak(output).set_should_end_session(False).response)

        else:
            return (handler_input.response_builder.set_should_end_session(True).response)

                

class NoIntentHandler(AbstractRequestHandler):
    """Handler for No Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.NoIntent")(handler_input)

    def handle(self, handler_input):
        output = static_responses.BYE_INTENT
        return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
class StopIntentHandler(AbstractRequestHandler):
    """Handler for Stop Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input)

    def handle(self, handler_input):
        output = static_responses.STOP_INTENT
        return (handler_input.response_builder.speak(output).set_should_end_session(True).response)
class GreetIntentHandler(AbstractRequestHandler):
    """Handler for Greet Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("greetintent")(handler_input)

    def handle(self, handler_input):
        output = static_responses.GREET_INTENT
        return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Greet Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("helpintent")(handler_input)

    def handle(self, handler_input):
        output = static_responses.HELP_INTENT
        return (handler_input.response_builder.speak(output).set_should_end_session(False).response)
class FallbackIntentHandler(AbstractRequestHandler):
    """Handler for Fallback Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.FallbackIntent")(handler_input)

    def handle(self, handler_input):
        output = static_responses.FALL_BACK
        return (handler_input.response_builder.speak(output).set_should_end_session(False).response)






class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors.
    If you receive an error stating the request handler chain is not found,
    you have not implemented a handler for the intent being invoked or
    included it in the skill builder below.
    """

    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = ("Sorry, I had trouble doing what you asked. "
                        "Please try again.")

        return (handler_input.response_builder.speak(speak_output).ask(
            speak_output).response)


            
sb = CustomSkillBuilder(api_client=DefaultApiClient())

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(CancelAppointment())
sb.add_request_handler(Appointmentqry())
sb.add_request_handler(YesIntentHandler())
sb.add_request_handler(Byeintent())
sb.add_request_handler(Rescheduleintent())
sb.add_request_handler(NoIntentHandler())
sb.add_request_handler(GreetIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(SessionResumedRequestHandler())
sb.add_request_handler(StopIntentHandler())
sb.add_exception_handler(CatchAllExceptionHandler())
handler = sb.lambda_handler()